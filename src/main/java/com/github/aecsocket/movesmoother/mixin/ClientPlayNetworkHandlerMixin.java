package com.github.aecsocket.movesmoother.mixin;

import net.minecraft.client.MinecraftClient;
import net.minecraft.client.network.ClientPlayNetworkHandler;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.network.NetworkThreadUtils;
import net.minecraft.network.packet.s2c.play.PlayerPositionLookS2CPacket;
import net.minecraft.network.packet.s2c.play.PositionFlag;
import net.minecraft.util.math.MathHelper;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.Set;

@Mixin(ClientPlayNetworkHandler.class)
public class ClientPlayNetworkHandlerMixin {
    @Final @Shadow private MinecraftClient client;

    @Inject(method = "onPlayerPositionLook", at = @At("HEAD"), cancellable = true)
    public void onPlayerPositionLook(PlayerPositionLookS2CPacket packet, CallbackInfo callback) {
        Set<PositionFlag> flags = packet.getFlags();
        PlayerEntity player = client.player;
        if (player == null) return;

        if (
            flags.contains(PositionFlag.X) && packet.getX() == 0
            && flags.contains(PositionFlag.Y) && packet.getY() == 0
            && flags.contains(PositionFlag.Z) && packet.getZ() == 0
        ) {
            NetworkThreadUtils.forceMainThread(packet, (ClientPlayNetworkHandler) (Object) this, client);

            float yaw = packet.getYaw();
            if (flags.contains(PositionFlag.X_ROT))
                yaw += player.getYaw();

            float pitch = packet.getPitch();
            if (flags.contains(PositionFlag.Y_ROT))
                pitch += player.getPitch();
            pitch = MathHelper.clamp(pitch, -90, 90);

            player.setYaw(yaw);
            player.prevYaw = packet.getYaw();

            player.setPitch(pitch);
            player.prevPitch = packet.getPitch();

            callback.cancel();
        }
    }
}
