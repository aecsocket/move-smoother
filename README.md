<div align="center">

<h1> <a href="https://gitlab.com/aecsocket/move-smoother">
<img src="icon.svg" height="64"> Move Smoother
</a> </h1>

[![Pipeline status](https://img.shields.io/gitlab/pipeline-status/aecsocket/move-smoother?branch=main)](https://gitlab.com/aecsocket/move-smoother/-/pipelines/latest)

</div>

Smooths various camera and movement issues in Minecraft, implemented as a Fabric mod.

# Features

- [x] Fixes "stuttering" when getting relative-teleported
- [x] Allows changing mouse sensitivity based on FOV
- [x] In-game menu to change options

# Usage

## Downloads

### Dependencies

* Fabric Loader `>=0.13.3`
* Java `>=17`
* [Fabric API](https://www.curseforge.com/minecraft/mc-mods/fabric-api) `>=0.47.9`
* [Mod Menu](https://www.curseforge.com/minecraft/mc-mods/modmenu) `>=3.1.0`
* [Cloth Config API](https://www.curseforge.com/minecraft/mc-mods/cloth-config) `>=6.2.57`

## Downloads for v1.6.2

### [Fabric](https://gitlab.com/api/v4/projects/26850725/jobs/artifacts/main/raw/paper/build/libs/move-smoother-1.6.2.jar?job=build)
